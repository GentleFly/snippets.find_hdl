#! /bin/sh


top_module=$1
prefix=$2
postfix=$3
file_of_stucturs=$4

OIFS=$IFS
IFS=';$'
list=( $(cat $file_of_stucturs) )
IFS=$OIFS

IFS=':'
for (( i=0; i < ${#list[*]}; i++ ))
do
    line=( ${list[$i]} )
    file[$i]=${line[0]}
    module[$i]=${line[1]}
    depends[$i]=$(echo -e ${line[2]} | sort -u )
    #echo ${file[$i]} ${module[$i]} ${depends[$i]} >> dbg.log
done
IFS=$OIFS
#echo '======================================================' >> dbg.log

cnt=0
dep=""
dep[0]="$top_module"
while [ ${#dep[*]} -gt $cnt ]
do
    for (( i=0; i < ${#module[*]}; i++ )) do
        if  [ "${dep[$cnt]}" = "${module[$i]}" ]
        then
            dep+=( ${depends[$i]} )
        fi
    done
    cnt=$(( $cnt + 1 ))
    #echo "DEBUG:$cnt"
    #echo "DEBUG:$cnt:${dep[*]}"
done
dep=( $(echo "${dep[@]}" | uniq ) )


cnt=0
for (( i=0; i < ${#dep[*]}; i++ )) do
    for (( j=0; j < ${#module[*]}; j++ )) do
        #echo "0:${dep[$i]} ?= ${module[$j]}" >> dbg.log
        if  [ "${dep[$i]}" = "${module[$j]}" ] ; then
            #echo "1:${dep[$i]} ?= ${module[$j]}" >> dbg.log
            if [ "${#depends[$j]}" -gt 0 ] ; then
                deps=( ${depends[$j]} )
                rule[$cnt]=$'\n'"${prefix}$(basename ${file[$j]})${postfix}:"
                #echo "3:${rule[$cnt]}" >> dbg.log
                for (( ii=0; ii < ${#deps[*]}; ii++ )) do
                    for (( jj=0; jj < ${#module[*]}; jj++ )) do
                        if  [ "${deps[$ii]}" = "${module[$jj]}" ] ; then
                            rule[$cnt]+=' \'$'\n\t'"${prefix}$(basename ${file[$jj]//$'\n'/' '}${postfix})"
                        fi
                    done
                done
                echo "${rule[$cnt]}" | uniq
                cnt=$(( $cnt + 1 ))
            fi
        fi
    done
done
#echo -e "${rule[@]}"

