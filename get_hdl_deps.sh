#! /bin/sh


top_module=$1
file_of_stucturs=$2

OIFS=$IFS
IFS=';$'
list=( $(cat $file_of_stucturs) )
IFS=$OIFS

#for (( i=0; i < ${#list[*]}; i++ ))
#do
#    echo $i ${list[$i]}
#done
#exit 0

IFS=':'
for (( i=0; i < ${#list[*]}; i++ ))
do
    line=( ${list[$i]} )
    file[$i]=${line[0]}
    module[$i]=${line[1]}
    depends[$i]=$(echo -e ${line[2]} | sort -u )
    #echo ${file[$i]} ${module[$i]} ${depends[$i]}
done
IFS=$OIFS

cnt=0
dep=""
dep[0]="$top_module"
while [ ${#dep[*]} -gt $cnt ]
do
    for (( i=0; i < ${#module[*]}; i++ )) do
        if  [ "${dep[$cnt]}" = "${module[$i]}" ]
        then
            dep+=( ${depends[$i]} )
        fi
    done
    cnt=$(( $cnt + 1 ))
    #echo "DEBUG:$cnt"
    #echo "DEBUG:$cnt:${dep[*]}"
done
dep=( $(echo "${dep[@]}" | uniq ) )

for (( i=0; i < ${#dep[*]}; i++ )) do
    for (( j=0; j < ${#module[*]}; j++ )) do
        if  [ "${dep[$i]}" = "${module[$j]}" ] ; then
            sources[$i]=${file[$j]}
        fi
    done
done
#for (( i=0; i < ${#sources[*]}; i++ )) do
#    echo "${sources[$i]}"
#done

echo "${sources[@]}" | tr ' ' '\n' | awk '!x[$0]++' | tac | tr '\n' ' '

