#!/bin/awk -f

#@include "lib.awk"

#BEGIN   { print "START" }

# delete empty string
/^\s*$/{next}

{
    gsub(/\s+/," ")
    gsub(/^\s+/, "")
    gsub(/^`.*$/, "")
}

# delete empty string
/^\s*$/{next}


# delete comments "single lines", "multi lines" or between quotes (display("blabla"))
{
    sc_schar  = index($0, "//")
    mc_schar  = index($0, "/*")
    qu_schar  = index($0, "\"")

    while ((0!=sc_schar) || (0!=mc_schar) || (0!=qu_schar)) {
        if (0 == sc_schar) {sc_schar = length($0) + 1}
        if (0 == mc_schar) {mc_schar = length($0) + 1}
        if (0 == qu_schar) {qu_schar = length($0) + 1}

        if        ((sc_schar < mc_schar) && (sc_schar < qu_schar)) {
            gsub(/\/\/.*$/, "")
        } else if ((mc_schar < sc_schar) && (mc_schar < qu_schar)) {
            delete_range("\\/\\*", "\\*\\/");
        } else if ((qu_schar < sc_schar) && (qu_schar < mc_schar)) {
            delete_range("\"", "[^\\\\]\"");
        }

        sc_schar  = index($0, "//")
        mc_schar  = index($0, "/*")
        qu_schar  = index($0, "\"")
    }
}

# delete empty string
/^\s*$/{next}


{print}
#END     { print "STOP"  }

