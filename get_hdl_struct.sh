#! /bin/sh

file=$1

cat ${file} | \
    awk -i ./lib.awk -f ./delete_comments.awk      | \
    awk -i ./lib.awk -f ./delete_trash.awk         | \
    awk -i ./lib.awk -f ./format.awk               | \
    awk -i ./lib.awk -f ./get.awk file_name=$file  | \
    uniq

