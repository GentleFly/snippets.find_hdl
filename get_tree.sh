#! /bin/sh

top_module=$1
file_of_stucturs=$2
#∷⇒∶⋮》∘∗∙Ⅰ▷∘
prefix='∘'

OIFS=$IFS
IFS=';$'
list=( $(cat $file_of_stucturs) )
IFS=$OIFS

IFS=':'
for (( i=0; i < ${#list[*]}; i++ ))
do
    line=( ${list[$i]} )
    file[$i]=${line[0]}
    module[$i]=${line[1]}
    depends[$i]=$(echo -e ${line[2]} | sort -u )
    #echo ${file[$i]} ${module[$i]} ${depends[$i]}
done
IFS=$OIFS

layer=''
cnt=0
dep=()
dep[0]="$top_module"
for (( j=0; j < ${#dep[*]}; j++ )) do
    layer="${dep[$cnt]//[^$prefix]}"
    for (( i=0; i < ${#module[*]}; i++ )) do
        if  [ "${dep[$cnt]//$prefix/}" = "${module[$i]}" ]  && [ "${#depends[$i]}" -gt 0 ];
        then
            layer="$layer$prefix"
            offset=$(( $j+1 ))
            dep=( ${dep[@]:0:$offset} ${depends[$i]//$'\n'/' '$layer} ${dep[@]:$offset} )
            #echo "DEBUG:first :${dep[@]:0:$offset}"
            #echo "DEBUG:second:${dep[@]:$offset}"
            #echo "DEBUG:$cnt:$i:${module[$i]}:${dep[$cnt]//$prefix/}:${depends[$i]//$'\n'/' '};"
        fi
    done
    cnt=$(( $cnt + 1 ))
done

for (( i=0; i < ${#dep[*]}; i++ )) do
    #echo "${dep[$i]}:$i"
    echo "${dep[$i]}"
done

