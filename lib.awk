#!/bin/awk -f

function delete_range (sr,er) {
    while (match($0, sr)) {
        sub_string = substr($0, 0, RSTART - 1)
        start_search_er = RSTART + RLENGTH - 1
        while (!match(substr($0, start_search_er), er)) {
            start_search_er = 1
            if(!getline) break
        }
        $0 = sub_string""substr($0, start_search_er + RSTART + RLENGTH - 1)
    }
}

