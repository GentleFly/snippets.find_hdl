
TOP_MODULE	=tb
NUMCPUS 	=$(shell grep -c '^processor' /proc/cpuinfo)

#DEPS_PATH	= ../tst/
DEPS_PATH	= ../../fpga/libv/src/ ../../fpga/libv/test/eth/dma/rx/
DEPS_WDIR	=./wrk/tmp
DEPS_FILE	=./wrk/hdl_source.list
DEPS_TREE	=./wrk/hdl_tree.txt
DEPS_MKFILE	=./wrk/hdl_deps.makefile

export TOP_MODULE DEPS_PATH DEPS_WDIR DEPS_FILE DEPS_TREE DEPS_MKFILE

all: find_hdl

out:
	make -j$(NUMCPUS) -f find_hdl.makefile

find_hdl: out
	@echo -en "\n"
	cat $(DEPS_FILE)
	@echo -en "\n"
	cat $(DEPS_TREE)
	@echo -en "\n"
	cat $(DEPS_MKFILE)
	@echo -en "\n"

clean:
	make -j$(NUMCPUS) -f find_hdl.makefile clean

