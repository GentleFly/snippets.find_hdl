#!/bin/awk -f

BEGIN {
    code_depth = 0
    #print "START"
}

{
    if ( match($0, /\<module\>/) \
      || match($0, /\<interface\>/) \
      || match($0, /\<class\>/) \
      || match($0, /\<task\>/) \
      || match($0, /\<function\>/) \
    ) {
        if ( code_depth == 0 ) {
            start=RSTART + RLENGTH + 1
            end=match(substr($0, RSTART + RLENGTH + 1), /[#(;]/) + 1
            define_name = gensub(/.*(\<\w+\>)\s*[#(;].*/, "\\1", 1, substr($0, start, end))
            #define_name = gensub(/\s+(\<\w+\>)\s*[(;#].*/, "\\1", 1, substr($0, RSTART + RLENGTH + 1))
            #define_name = gensub(/\s*(\<\w+\>)\s*/, "\\1", 1, substr($0, RSTART + RLENGTH))
            #define_name = gensub(/^(\w+\>).*/, "\\1", 1, substr($0, RSTART + RLENGTH + 1))
            $0=""
            print file_name":"define_name":"
            #{printf "dbg:%s\n", $0}
            #{printf "dbg:%s\n", substr($0, RSTART + RLENGTH + 1)}
            #{printf "dbg:%s\n", define_name}
            #{printf "dbg:%d\n", match(substr($0, RSTART + RLENGTH + 1), /[#(;]/)}
        }
        code_depth++
    } else if ( match($0, /\<endmodule\>/) \
             || match($0, /\<endinterface\>/) \
             || match($0, /\<endclass\>/) \
             || match($0, /\<endtask\>/) \
             || match($0, /\<endfunction\>/) \
    ) {
        code_depth--
        if ( code_depth == 0 ) {
            $0=""
            print ";"
        }
    }
}

{
    if ( code_depth ) {
        gsub(/\<virtual\>/, "")
        gsub(/\<repeat\>/, "")
        gsub(/\<while\>/, "")
        gsub(/\<if\>/, "")
        gsub(/\<case\>/, "")
        gsub(/\<endcase\>/, "")
        gsub(/\<foreach\>/, "")
        gsub(/\<return\>/, "")
        gsub(/\<wait\>/, "")

        delete_range("\\<pure\\>",";")
        delete_range("\\<modport\\>",";")
        delete_range("\\<parameter\\>",";")
        delete_range("\\<localparam\\>",";")
        delete_range("\\<logic\\>",";")
        delete_range("\\<initial\\>",";")
        delete_range("\\<int\\>",";")
        delete_range("\\<integer\\>",";")
        delete_range("\\<reg\\>",";")
        delete_range("\\<wire\\>",";")
        delete_range("\\<wor\\>",";")
        delete_range("\\<tri1\\>",";")
        delete_range("\\<genvar\\>",";")
        delete_range("\\<bit\\>",";")
        delete_range("\\<shortreal\\>",";")
        delete_range("\\<real\\>",";")
        delete_range("\\<mailbox\\>",";")

        delete_range("\\<task\\>",";")
        gsub(/\<endtask\>/, "")

        delete_range("\\<function\\>",";")
        gsub(/\<endfunction\>/, "")

        delete_range("\\<class\\>",";")
        gsub(/\<endclass\>/, "")

        delete_range("\\<module\\>",";")
        gsub(/\<endmodule\>/, "")

        delete_range("\\<interface\\>",";")
        gsub(/\<endinterface\>/, "")

        if (    match($0, /^\w+\>\s*\#\s*\(.*\)\s*\<\w+\>\s*\(.*\)\s*\;/) \
            ||  match($0, /^\w+\>\s+\<\w+\>\s*\(.*\)\s*\;/)  \
            ||  match($0, /^\w+\>\s*\#\s\(.*\)\s*\<\w+\>\s*\;/) \
            ||  match($0, /^\w+\>\s+\<\w+\>\s*\;/) \
            ||  match($0, /^\w+\>\s*\(.*\)\s*\;/)  \
        ) {
            sub(/\W+.*$/, "")
            print
        }
    }
}

#{printf "%2d:%s\n", code_depth, $0}
#{print}
#END { print "STOP" }

