#!/bin/awk -f

#BEGIN   { print "START" }

# delete attribute: (* attribute = x *)
delete_range("\\(\\*","\\*\\)")

{
    delete_range("\\[","\\]")
    delete_range("\\{","\\}")
    gsub(/:\s+\<\w+\>/, "")
    gsub(/\s+$/, "")
    gsub(/^\s+/, "")
}

# temporary
#delete_range("\\<task\\>","\\<endtask\\>");
#delete_range("\\<function\\>","\\<endfunction\\>");

# delete empty string
/^\s*$/{next}


{print}
#END     { print "STOP"  }

