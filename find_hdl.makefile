
DEPS_WDIR	?=./tmp
TOP_MODULE	?=tb
DEPS_PATH	?=./
DEPS_FILE	?=$(DEPS_WDIR)/hdl.deps
DEPS_TREE	?=$(DEPS_WDIR)/hdl.tree
DEPS_MKFILE	?=$(DEPS_WDIR)/deps.makefile

HDL_FILES	=$(shell find $(DEPS_PATH) -regex '.*\.s?v')
HDL_STRUCT	=$(foreach name,$(notdir $(HDL_FILES)),$(DEPS_WDIR)/$(name).struct)
VPATH		=$(sort $(dir $(HDL_FILES)) )

POSTFIX		='.touch'
PREFIX		="$(DEPS_WDIR)/"

all: out

$(DEPS_WDIR)/cteate_dir.touch:
	mkdir -p $(DEPS_WDIR)/
	touch $@

$(HDL_STRUCT): $(DEPS_WDIR)/%.struct : %  $(DEPS_WDIR)/cteate_dir.touch
	@tmpfile=`mktemp`; \
		./get_hdl_struct.sh $< > $$tmpfile ;\
		cmp -s $$tmpfile $@; \
		RETVAL=$$?; \
		if [ $$RETVAL -eq 0 ]; then \
			echo "Structure not changed: $<"; \
		else \
			echo "./get_hdl_struct.sh $< $@"; \
			cp -u $$tmpfile $@; \
		fi; \
		rm -f $$tmpfile

$(DEPS_WDIR)/struct: $(HDL_STRUCT)
	cat $(DEPS_WDIR)/*.struct > $@

$(DEPS_FILE): $(DEPS_WDIR)/struct
	./get_hdl_deps.sh $(TOP_MODULE) $< > $@

$(DEPS_TREE): $(DEPS_WDIR)/struct
	./get_tree.sh $(TOP_MODULE) $< > $@

$(DEPS_MKFILE): $(DEPS_WDIR)/struct
	./get_mk_deps.sh $(TOP_MODULE) $(PREFIX) $(POSTFIX) $< > $@

out: $(DEPS_FILE) $(DEPS_TREE) $(DEPS_MKFILE)
	@echo -e "\n$(DEPS_FILE)\n$(DEPS_TREE)\n$(DEPS_MKFILE)\n"

clean:
	rm -rf $(DEPS_FILE)
	rm -rf $(DEPS_MKFILE)
	rm -rf $(DEPS_TREE)
	rm -rf $(DEPS_WDIR)

