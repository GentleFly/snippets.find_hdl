#!/bin/awk -f
# ./format.awk ../tst/bob.v
# awk -f format.awk ../tst/bob.v

#BEGIN { print "START" }

{
    line = ""
    gl=1
    while (match($0, /[^;]\s*$/)){
        line = line" "$0
        gl = getline
        if(!gl) {break}
    }
    if (gl) {line = line" "$0}
    $0 = line
    gsub(/^\s+/, "")
}

{
    gsub(/;[^$]/, ";\n")
    #$0 = gensub(/\<module\>\s+(\<\w*\>)/, "\n\\1\n", "g", $0)
    gsub(/\<module\>.+;/, "\n&")
    gsub(/\<endmodule\>\s*/, "&\n")

    gsub(/\<interface\>.+;/, "\n&")
    gsub(/\<endinterface\>s*/, "\n&\n")

    gsub(/\<class\>.+;/, "\n&")
    gsub(/\<endclass\>\s*/, "\n&\n")

    gsub(/\<function\>.+;/, "\n&")
    gsub(/\<endfunction\>\s*/, "&\n")

    gsub(/\<task\>.+;/, "\n&")
    gsub(/\<endtask\>\s*/, "&\n")

    gsub(/\<generate\>\s*/, "\n")
    gsub(/\<endgenerate\>\s*/, "\n")

    gsub(/\<begin\>\s*/, "\n")
    gsub(/\<end\>\s*/, "\n")

    #delete_range("\\<if\\>",")\\s*")
    gsub(/\<if\>\s*\(\s*/, "\n")
    gsub(/\<else\>\s*/, "\n")
}

{print}
#END { print "STOP" }

